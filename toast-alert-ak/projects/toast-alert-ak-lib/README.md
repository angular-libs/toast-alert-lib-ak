# ToastAlertLib

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

# Git Repo

Sample and code is uploaded on [BitBucket](https://babitaraheja@bitbucket.org/angular-libs/toast-alert-lib-ak.git)


## Installation

To install the package

> npm i 'toast-alert-ak-lib'

> npm i --save @fortawesome/fontawesome-free

1. Open 'app.module.ts'
2. `import { ToastAlertAkLibModule } from 'toast-alert-ak-lib';`
3. in the Import add module `ToastAlertAkLibModule`
4. Open angular.json
5. Add a styles in angular.json
        "node_modules/@fortawesome/fontawesome-free/css/all.min.css"

### Use Toast Notification

4.  Open 'app.component.html'
5.  Add selector: `<lib-toast-message></lib-toast-message>`
6.  in the TS file. use a service in the constructor
7.  constructor(private notify: ToastNotifyService)
8.  Code
    `Success() {
    this.notify.success('Success', 'Record saved successfully', 'https://www.voicesofyouth.org/sites/voy/files/images/2020-07/success.jpg');
    }
    Warning(){
    this.notify.warning('Warning', 'Valid information not passed');
    }
    Error(){
    this.notify.error('Error', 'Record not saved');
    }
    Information(){
    this.notify.information('Information', 'You are a good person');
    }
    Question(){
    this.notify.question('Question', 'are you fine?');
    }

    loadingVal = "1";
    Loading(){
       his.notify.loading(this.loadingVal, null, 'Saving your records');
    }

    //To stop the loader
    removeLoading(){
        this.notify.stopLoading(this.loadingVal);
    }

    //If you want to show success, error, warning message and stop the loader direct

    Success(){
        this.notify.success('Success', 'Record saved successfully', this.loadingVal, 'https://www.voicesofyouth.org/sites/voy/files/images/2020-07/success.jpg');
    }`


### Use Alert Message
4.  Open 'app.component.html'
5.  Add selector: `<lib-alert-message></lib-alert-message>`
6.  in the TS file. use a service in the constructor
7.  constructor(private alert: AlertNotifyService)
8.  Code
    `
    alertMess() {
        this.alert.sendAlert('header', 'mess', [{
        name: 'Test',
        type: ButtonColorEnum.Success
        }, 
        { name: 'Cancel', 
        type: ButtonColorEnum.Other }
        ]);

        //If you want to know what the user has clicked.
        this.alert.$alertNotificationReceive.pipe(take(1)).subscribe(res => {
        alert(res.name);
        });
    }`


## NOTE
If you need some additional changes 

raheja.abhi@hotmail.com