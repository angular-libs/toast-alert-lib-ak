import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AlertMessageComponent } from './alert-message/alert-message.component';
import { AlertNotifyService } from './alert-message/alert-notify.service';
import { ToastMessageComponent } from './toast-message/toast-message.component';
import { ToastNotifyService } from './toast-message/toast-message.service';



@NgModule({
  declarations: [AlertMessageComponent, ToastMessageComponent],
  imports: [
    CommonModule
  ],
  exports: [ToastMessageComponent, AlertMessageComponent],
  providers: [ToastNotifyService, AlertNotifyService]
})
export class ToastAlertAkLibModule { }
