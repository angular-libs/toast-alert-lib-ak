export enum ButtonColorEnum{
    Success = "my-color-success",
    Error = "my-color-error",
    Warning = "my-color-warning",
    Information = "my-color-info",
    Link = "my-color-link",
    Success_Light = "my-color-primary",
    Other = ""
}
