import { ToastNotifyIconEnum } from "./toast-notify-icon.enum";

export class ToastNotifyModel {
    constructor(public id?: string, public h?: string, public m?: string, public mIcon?: ToastNotifyIconEnum, public removeLoading?: boolean, public imgPath?: string) {

        this.header = h ?? 'Question';
        this.message = m ?? "Lorem Lorem Lorem Lorem";
        this.icon = mIcon ?? ToastNotifyIconEnum.question;
        this.isLoading = mIcon === ToastNotifyIconEnum.loading;
        this.rmLoading = removeLoading ?? false;
        this.imgSrc = imgPath ?? '';
    }
    public header?: string;
    public message?: string;
    public sClass?: string;
    public isLoading?: boolean;
    public icon?: ToastNotifyIconEnum;
    public rmLoading?: boolean;
    public imgSrc?: string;
    get alertColor(): alertColor {
        switch (this.icon) {
            case ToastNotifyIconEnum.success:
                return {
                    bgColor: '#D6EDDB',
                    color: '#449B57'
                };
            case ToastNotifyIconEnum.warning:
                return {
                    bgColor: '#FFF0CF',
                    color: '#D58F00'
                };
            case ToastNotifyIconEnum.error:
                return {
                    bgColor: '#F7D8DD',
                    color: '#D73951'
                };
            case ToastNotifyIconEnum.info:
                return {
                    bgColor: '#CEE6FE',
                    color: '#0578EB'
                };
            case ToastNotifyIconEnum.question:
                return {
                    bgColor: '#EEEEEE',
                    color: '#797979'
                };
            case ToastNotifyIconEnum.loading:
                return {
                    bgColor: '#FFF5EC',
                    color: '#E86F00'
                };

        }
    }

}

export interface alertColor {
    bgColor: string;
    color: string;
}