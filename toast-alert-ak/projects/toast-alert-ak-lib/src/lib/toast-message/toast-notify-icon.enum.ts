export enum ToastNotifyIconEnum {
    success = "fa-check-circle",
    error = "fa-times-circle",
    warning = "fa-exclamation-circle",
    info = "fa-info-circle",
    question = "fa-question-circle",
    loading = "loading"
}