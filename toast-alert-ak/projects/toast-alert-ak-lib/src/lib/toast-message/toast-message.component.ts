import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { ToastNotifyService } from './toast-message.service';
import { ToastNotifyModel } from './toast-notify.model';

@Component({
  selector: 'lib-toast-message',
  templateUrl: './toast-message.component.html',
  styleUrls: ['./toast-message.component.css']
})
export class ToastMessageComponent implements OnInit {


  messages: ToastNotifyModel[] = [];

  constructor(private notify: ToastNotifyService) { }

  ngOnInit(): void {
    this.notify.$notification.subscribe(res => {
      if (res) {
        if (!(res.id)) {
          this.addItem(res);
        }
        else {
          const record = this.messages.find(x => x.id === res.id);
          if (record) {
            this.removeItem(record);
          }
          if (!res.rmLoading)
            this.addItem(res);
        }
      }
    });
  }


  removeNotification(mess: ToastNotifyModel) {
    this.removeItem(mess);
  }


  private addItem(mess: ToastNotifyModel): void {
    mess.sClass = "notification-animation-in";
    mess.message = mess.message;
    this.messages.push(mess);
    if (!mess.isLoading) {
      interval(5000).pipe(take(1)).subscribe(x => {
        this.removeItem(mess);
      });
    }
  }

  private removeItem(mess: ToastNotifyModel): void {
    mess.sClass = "notification-animation-out";
    interval(400).pipe(take(1)).subscribe(x => {
      const item = this.messages.indexOf(mess);
      if (item > -1) {
        this.messages.splice(item, 1);
      }
    });
  }

}
