import { Component } from '@angular/core';
import { AlertNotifyService, ButtonColorEnum, ToastNotifyService } from 'toast-alert-ak-lib';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private notify: ToastNotifyService, private alert: AlertNotifyService) {

  }

  notifyMessage() {
    this.notify.loading('1', 'test', 'test');
  }
  sendMessage() {
    this.notify.success("header0", "value", "https://www.goalcast.com/wp-content/uploads/2021/01/Motivational-quotes-about-success-1024x538.png");
  }

  alertMess() {
    this.alert.sendAlert('header', 'mess', [{
      name: 'Test',
      type: ButtonColorEnum.Success
    }, { name: 'Cancel', type: ButtonColorEnum.Other }]);
    this.alert.$alertNotificationReceive.pipe(take(1)).subscribe(res => {
      alert(res.name);
    });
  }
}
