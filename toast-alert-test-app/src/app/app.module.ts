import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastAlertAkLibModule } from 'toast-alert-ak-lib';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ToastAlertAkLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
